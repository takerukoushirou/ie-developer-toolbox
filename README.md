# Internet Explorer Developer Toolbox

A collection of tools for developing with or testing on IE, especially older version for which most tools are no longer available.

## Internet Explorer Developer Toolbar

Originally developed by Microsoft for Internet Explorer 6 and 7, also known as F12 Toolbar.

Internet Explorer 8 and newer already have the toolbar known as *Internet Explorer Developer Tools* built in.

- Version 1.00.2189.0, works on latest version of Internet Explorer 7.
